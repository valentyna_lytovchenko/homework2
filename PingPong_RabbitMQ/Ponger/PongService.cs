﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;

namespace Ponger
{
    public class PongService
    {
        private readonly MessageProducerScope _messageProducerScope;
        private readonly MessageConsumerScope _messageConsumerScope;

        public PongService(MessageProducerScopeFactory messageProducerScopeFactory,
                           MessageConsumerScopeFactory messageConsumerScopeFactory)
        {

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue_test",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue_test",
                RoutingKey = "response"
            });

        }
    }
}
