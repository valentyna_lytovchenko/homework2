﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public MessageConsumerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageConsumerScope(_connectionFactory, messageScopeSettings);
        }

        public MessageConsumerScope Connect(MessageScopeSettings messageScopeSettings)
        {
            var mqConsumerScope = Open(messageScopeSettings);
            mqConsumerScope.MessageConsumer.Connect();

            return mqConsumerScope;
        }

    }
}
