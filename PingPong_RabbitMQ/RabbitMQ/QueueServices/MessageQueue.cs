﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue
    {
        private readonly IConnection _connection;        

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings settings)
            :this(connectionFactory)
        {
            DeclareExchange(settings.ExchangeName, settings.ExchangeType);

            if (settings.QueueName != null)
            {
                BindQueue(settings.ExchangeName, settings.RoutingKey, settings.QueueName);
            }
        }

        public IModel Channel { get; protected set; }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
