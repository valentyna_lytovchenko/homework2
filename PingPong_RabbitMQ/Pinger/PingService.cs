﻿using RabbitMQ.Wrapper.QueueServices;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Client;

namespace Pinger
{
    public class PingService
    {
        private readonly MessageProducerScope _messageProducerScope;
        private readonly MessageConsumerScope _messageConsumerScope;

        public PingService(MessageProducerScopeFactory messageProducerScopeFactory,
                           MessageConsumerScopeFactory messageConsumerScopeFactory) {

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue_test",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue_test",
                RoutingKey = "response"
            });

        }
    }
}
